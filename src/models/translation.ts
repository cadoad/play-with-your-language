import {Tag} from "./tag";
/**
 * Created by adric on 09/07/2017.
 */
export class Translation{
  public translation_fr : string;
  public translation_en : string;
  public updated_date : string;
  public tags : Tag[]
}

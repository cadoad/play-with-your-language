import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {ListPageModule} from "../pages/list/list.module";
import {TensesPageModule} from "../pages/tenses/tenses.module";
import {PlayPageModule} from "../pages/play/play.module";

// New imports to update based on AngularFire2 version 4
import { AngularFireModule } from "angularfire2";
import { TranslationProvider } from '../providers/translation/translation';
import {AngularFireDatabaseModule} from "angularfire2/database/database.module";

export const firebaseConfig = {
  apiKey: "AIzaSyAksr_qqUKDmVdjKNLtF5eZGaYui2-A5Vk",
  authDomain: "playwithyourlanguage.firebaseapp.com",
  databaseURL: "https://playwithyourlanguage.firebaseio.com",
  projectId: "playwithyourlanguage",
  storageBucket: "playwithyourlanguage.appspot.com",
  messagingSenderId: "568713978944"
};

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    ListPageModule,
    TensesPageModule,
    PlayPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    TranslationProvider
  ]
})
export class AppModule {}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TensesPage } from './tenses';

@NgModule({
  declarations: [
    TensesPage,
  ],
  imports: [
    IonicPageModule.forChild(TensesPage),
  ],
  exports: [
    TensesPage
  ]
})
export class TensesPageModule {}

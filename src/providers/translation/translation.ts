import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {FirebaseListObservable} from "angularfire2/database/firebase_list_observable";
import {AngularFireDatabase} from "angularfire2/database/database";

/*
  Generated class for the TranslationProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class TranslationProvider {

  items: FirebaseListObservable<any[]>;

  constructor(private db: AngularFireDatabase) {}

  public getAllTranslations() {
    console.log('get');
    this.items = this.db.list('/items');
    console.log(this.items);
    // return db.list('/items');
  }


}
